from distutils.log import debug
from app import create_app
from config import setting
from app.views import users


app = create_app(setting)
app.blueprint(users)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000',debug=True)