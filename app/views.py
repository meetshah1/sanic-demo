
from .db import User
from bson import ObjectId
from sanic import Blueprint
from sanic.exceptions import NotFound
from sanic.response import json

users = Blueprint('content_user', url_prefix='/')


@users.route("/", methods=["POST"])
async def create_user(request):
    user = request.json
    user["_id"] = str(ObjectId())
    new_user = await User.insert_one(user)
    created_user = await User.find_one(
        {"_id": new_user.inserted_id}, as_raw=True
    )
    return json(created_user)

@users.route("/", methods=["GET"])
async def list_users(request):
    users = await User.find(as_raw=True)
    return json(users.objects)


@users.route("/<id>", methods=["GET"])
async def show_user(request, id):
    user = await User.find_one(
        {"_id": id}, as_raw=True
    )
    if user:
        return json(user)

    raise NotFound(f"User {id} not found")

@users.route("/<id>", methods=["PUT"])
async def update_user(request, id):
    user = request.json
    existing_user = await User.find_one({"_id": id}, as_raw= True)
    if not existing_user:
        raise NotFound(f"User {id} not found")

    update_result = await User.update_one({"_id": id}, {"$set": user})
    if update_result.modified_count == 1:
        updated_user = await User.find_one({"_id": id}, as_raw=True)
        return json(updated_user)
    else: 
        return json(existing_user)


@users.route("/<id>", methods=["DELETE"])
async def delete_user(request, id):
    delete_result = await User.delete_one({"_id": id})

    if delete_result.deleted_count == 1:
        return json({}, status=204)

    raise NotFound(f"User {id} not found")