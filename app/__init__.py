from sanic import Sanic
from sanic_motor import BaseModel

def create_app(config):
    sanic_app = Sanic(__name__)
    sanic_app.config.update_config(config)
    BaseModel.init_app(sanic_app)
    
    return sanic_app